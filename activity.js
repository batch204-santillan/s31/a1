/*
	QUIZ SECTION:

	a. What directive is used by Node.js in loading the modules it needs?
		► require

	b. What Node.js module contains a method for server creation?
		► http module

	c. What is the method of the http object responsible for creating a server using Node.js?
		► createServer ()

	d. What method of the response object allows us to set status codes and content types?
		► writeHead ()

	e. Where will console.log() output its contents when run in Node.js?
		► terminal

	f. What property of the request object contains the address' endpoint?
		► url or req.url == '/sampleAddress'

*/


/*
	CODE SECTION
*/

const http = require ('http');
const port = 3000;

const server = http.createServer((req , res) => {
	if (req.url == '/login') {
		res.writeHead(200 , {'Content-Type': 'text/plain'}) ;
		res.end ("Welcome to the login page!") ;
	}

	
	else {
		res.writeHead (404, {'Content-Type': 'text/plain'}) ;
		res.end ("I'm sorry the page you are looking for cannot be found")
	}

});

server.listen(port);

console.log (`Server is now accessible at localhost: ${port}. ` );